/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'BizDash.Application',

    name: 'BizDash',

    requires: [
        // This will automatically load all classes in the BizDash namespace
        // so that application classes do not need to require each other.
        'BizDash.*'
    ],

    // The name of the initial view to create.
    mainView: 'BizDash.view.main.Main'
});
