/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('BizDash.Application', {
    extend: 'Ext.app.Application',

    name: 'BizDash',

    requires: [
        'BizDash.model.Product',
        'BizDash.model.Location',
        'BizDash.model.Sale',
        'BizDash.model.User',
        'BizDash.model.BarChart',
        'BizDash.model.WebSiteVisitor'
    ],

    views: [
        // TODO: add views here
        'product.ProductGrid',
        'dashboard.Overview',
        'chart.BarChart',
        'chart.SiteVisits',
        'chart.StockLevelPie',
        'user.UsersView'
    ],

    controllers: [
        'Main',
        'Root'
        // TODO: add controllers here
    ],

    stores: [
        'Users',
        'Products',
        'Locations',
        'Navigation',
        'users.Admins',
        'users.Customers',
        'users.Suppliers',
        'BarChart',
        'WebSiteVisitors'
    ],

    launch: function() {
        // var usersStore = Ext.getStore('Users');
        // usersStore.load(function(){
        //     var adminStore = Ext.create('BizDash.store.users.Admins', {});
        //     var customerStore = Ext.create('BizDash.store.users.Customers', {});
        //     var supplierStore = Ext.create('BizDash.store.users.Suppliers', {});
        //     console.log(usersStore.getCount()); // 4
        //     console.log(adminStore.getCount()); // 2
        //     console.log(customerStore.getCount()); // 1
        //     console.log(supplierStore.getCount()); // 1
        // });
        // console.log(usersStore);

        // Ext.Ajax.request({
        //     url: 'data/user.json',
        //     success: function(response, options){
        //         var user = Ext.decode(response.responseText);
        //         console.log(user);
        //     },
        //     failure: function(response, options){
        //         console.log('The request failed! Response Code: ' + response.status);
        //     }
        // });

        Ext.getStore('Users').load(function(records, operation, success){
            console.log('Num. users = '+Ext.getStore('Users').getCount()); // 4
            console.log(records); // [ ...record instances... ]
            // Ext.getStore('Users').getAt(2).set('Email', 'kevin@freemail.com');
            // Ext.getStore('Users').removeAt(3);
            // Ext.getStore('Users').sync();
        });
        // Ext.getStore('Users').add({
        //     Name: 'Stuart',
        //     Email: 'stuart@gmail.com',
        //     Role: 'Customer',
        //     TelNumber: ' 0330 122 2800'
        // });
        // Ext.getStore('Users').sync();

        BizDash.model.User.load(1, {
            success: function(userRecord) {
                userRecord.sales().load(function() {
                    console.log('User: ', userRecord.get('Name')); // Joe Bloggs
                    console.log('Sales: ', userRecord.sales().getCount()); // 2
                    console.log(userRecord.sales().getAt(0));
                });
            }
        });
        // BizDash.model.Sale.load(1, {
        //     success: function(saleRecord) {
        //         saleRecord.getUser(function(userRecord){
        //             console.log('UserSale: '+userRecord.get('Name')) // Joe Bloggs
        //         });
        //     }
        // });

        // BizDash.model.Product.load(1, {
        //     success: function(productRecord) {
        //         console.log(productRecord);
        //         productRecord.locations().load(function() {
        //             console.log('Locations: '+productRecord.locations().getCount()); // 2
        //         });
        //     }
        // });

    },

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});

Ext.Loader.setConfig({
    paths: {
        BizDash  : 'app',
        Overrides: 'overrides'
    }
});
