Ext.define('BizDash.config.Config', {
	extend: 'Ext.util.Observable',

	config: {
		version: '0.0.1-0'
	},

	getBuildNumber: function () {
		var versionSplit = this.getVersion().split('-');
		return versionSplit[1];
	}
});