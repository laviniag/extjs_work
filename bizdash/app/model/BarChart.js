Ext.define('BizDash.model.BarChart', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'value',
            type: 'int'
        }
    ]
});