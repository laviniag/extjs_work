Ext.define('BizDash.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'BizDash.model'
    }
});
