Ext.define('BizDash.model.Personnel', {
    extend: 'BizDash.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
