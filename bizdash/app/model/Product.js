// sencha generate model Product Name:string,Description:string,Quantity:int,Price:float
Ext.define('BizDash.model.Product', {
	//extend: 'BizDash.model.Base',
	extend: 'Ext.data.Model',

	requires: [
		'BizDash.model.field.Money',
		'Ext.data.validator.Presence',
		'Ext.data.validator.Length'
	],

	proxy: {
		type  : 'rest',
		api: {
            read: 'proxy/products.php',
            update: 'proxy/products.php'
        },
		reader: {
            type: 'json'
        },
        writer: {
            type          : 'json',
            writeAllFields: true
        }
	},

	// manyToMany: [
	// 	'Location',
	// ],
    hasMany: {
		model: 'BizDash.model.Location',
		name: 'locations',
		foreignKey: 'itemId'
	},

	fields: [
		{ name: 'Name', type: 'string' },
		{ name: 'Description', type: 'string' },
		{ name: 'Quantity', type: 'int' },
		{ name: 'Price', type: 'money' },
		{ name: 'Price2', type: 'money',
            convert: function(val, rec) {
                return rec.get('Price').toFixed(2);
            },
            depends: ['Price']
		},
		{ name   : 'StockValueConvert',
			type   : 'money',
			convert: function(val, rec) {
				return (rec.get('Quantity') * rec.get('Price')).toFixed(2);
			},
			depends: ['Price', 'Quantity']
		},
		{ name     : 'StockValueCalculate',
			type     : 'money',
			calculate: function(data) {
				return data.Quantity * data.Price;
			}
		},
		{ name: 'HistoricSales',
			type: 'auto',
			defaultValue: [4, 9, 12, 66, 9]
		}
	],

	validators: {
		Name    : [
			{ type: 'presence'	},
			{ type: 'length',	min : 3	}
		],
		Quantity: 'presence'
	}
});
