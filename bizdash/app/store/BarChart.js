Ext.define('BizDash.store.BarChart', {
    extend: 'Ext.data.Store',

    model: 'BizDash.model.BarChart',

    proxy: {
        type : 'ajax',
        url : 'data/barchart.json',
        reader: {
            type : 'json',
            rootProperty: 'rows'
        }
    },
    autoLoad: true
});