/**
 * BizDash.store.Locations
 */
Ext.define('BizDash.store.Locations', {
	extend: 'Ext.data.Store',

	model: 'BizDash.model.Location',

	autoLoad: true,

	proxy: {
		type  : 'ajax',
		url   : 'proxy/locations.php',
		reader: {
			type        : 'json',
			rootProperty: 'rows'
		}
	}

});