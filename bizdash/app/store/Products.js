/**
 * BizDash.store.Products
 */
Ext.define('BizDash.store.Products', {
	extend: 'Ext.data.Store',

	model: 'BizDash.model.Product',

	autoLoad: true,

	proxy: {
		type  : 'ajax',
		url   : 'proxy/products.php',
		reader: {
			type        : 'json',
			rootProperty: 'rows'
		}
	}

});