Ext.define('BizDash.view.chart.BarChart', {
    extend: 'Ext.chart.Chart',

	xtype: 'chart-BarChart',

    layout: 'fit',
    height: 500,

	items: {
        xtype: 'chart',
		animate: true,
		store  : 'BarChart',
		axes   : [
			{
				type    : 'numeric',
				position: 'left',
				fields  : ['value'],
				title   : 'Value',
                minimum       : 0,
                maximum       : 150,
                majorTickSteps: 10
			},
			{
				type    : 'category',
				position: 'bottom',
				fields  : ['name'],
				title   : 'Name'
			}
		],
		series : [
			{
				type  : 'bar',
				axis  : 'bottom',
				xField: 'name',
				yField: 'value'
			}
		]
	}
});