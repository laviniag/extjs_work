Ext.define('BizDash.view.chart.SiteVisits', {
	// extend: 'Ext.chart.CartesianChart',
    extend: 'Ext.chart.CartesianChart',

	xtype: 'chart-SiteVisits',

    layout: 'fit',
    height: 500,

    items: {
        xtype: 'chart',
		animate: true,
		store  : 'WebSiteVisitors',
		series : [
            {
                type: 'line',
                lineWidth: 1,
                fill: true,
                axis: 'bottom',
                xField: 'Time',
                yField: 'Visitors',
                style: {
                    'stroke-width': 1,
                    stroke: 'rgb(17, 95, 166)'
                }
            },		{
				type  : 'line',
				smooth: false,
				axis  : 'left',
				xField: 'Time',
				yField: 'Visitors',
                style: {
                    'stroke-width': 2,
                    stroke: 'rgb(158, 95, 102)'
                }
			}
		],
		axes   : [
			{
				type          : 'numeric',
				grid          : true,
				position      : 'left',
				fields        : ['Visitors'],
				title         : 'Visitors',
				minimum       : 0,
				maximum       : 200,
				majorTickSteps: 5
			},
			{
				type          : 'numeric',
				position      : 'bottom',
				fields        : ['Time'],
				title         : 'Time',
				minimum       : 0,
				maximum       : 20,
				decimals      : 0,
				constrain     : true,
				majorTickSteps: 20
			}
		]
	}
});