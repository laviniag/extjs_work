Ext.define('BizDash.view.chart.StockLevelPie', {
	extend: 'Ext.chart.PolarChart',

	xtype: 'chart-StockLevelPie',

    layout: 'fit',
    height: 500,

	items: {
        xtype: 'polar',
		animate     : true,
		store       : 'Products',
		interactions: 'rotate',
		series      : {
			type  : 'pie',
			label : {
				field  : 'Name',
				display: 'rotate'
			},
			xField: 'Quantity',
			donut : 20
		}
	}
});