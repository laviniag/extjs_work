Ext.define('BizDash.view.dashboard.Overview', {
    extend: 'Ext.container.Container',

    xtype: 'dashboard-Overview',

    requires: [
        'BizDash.view.dashboard.OverviewController',
        'BizDash.view.dashboard.OverviewModel'
    ],
    controller: 'Overview',
    viewModel: {
        type: 'Overview'
    },
    layout: {
        type: 'hbox',
        align: 'stretchmax'
    },
    items: [
        {
            flex: .3,
            title: 'Today\'s Events'
        },
        {
            flex: .3,
            title: 'Messages'
        },
        {
            width: 200,
            title: 'Notes'
        }
    ]
});