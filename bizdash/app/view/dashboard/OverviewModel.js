/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('BizDash.view.dashboard.OverviewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.Overview',

    data: {
        name: 'BizDash'
    }

    //TODO - add data, formulas and/or methods to support your view
});