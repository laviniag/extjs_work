/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('BizDash.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],

    alias: 'controller.main',

    onClickButton: function() {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        this.getView().getButton().disable();
    },

    onConfirm: function(choice) {
        if (choice === 'yes') {
            this.getView().getButton().fireEvent('confirmed', choice)
        }
    },

    onConfirmed: function(choice) {
        console.log('The CONFIRMED event was fired');
        this.getView().getButton().setStyle('background-color', 'red');
    },

    init: function() {
        var button = this.getView().query('button[text="PRESS"]')[0];

        console.log('The getView()');

        if (button) {
            //button.on('mouseover', 'onMouseOver');

            button.on({
                mouseover: 'onMouseOver',
                mouseout : 'onMouseOut',
                click    : { fn: 'onClickButton', single: true},
                confirmed: 'onConfirmed',
                scope    : this
            });

            var map = new Ext.util.KeyMap({
                target: this.getView().getEl(),
                key   : Ext.event.Event.ENTER,
                fn    : this.onEnterPress,
                scope : this
            });

            var el = this.getView().getEl();
            el.on('tap', function() {
                console.log('The Viewport was tapped/clicked.');
            });
        }
    },

    onMouseOver: function() {
        console.log('Button Mouseover Event Fired');
    },

    onMouseOut: function() {
        console.log('Button Mouseout Event Fired');
    },

    onEnterPress: function() {
        console.log('ENTER key was pressed');
    },

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    setBackground: function () {
        this.getView().getButton().fireEvent('confirmed', 1);
    }
});
