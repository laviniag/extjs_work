Ext.define('BizDash.view.product.ProductForm', {
	extend: 'Ext.form.Panel',

	requires: [
		'BizDash.view.product.ProductFormController',
		'BizDash.view.product.ProductFormModel'
	],

	xtype: 'product-ProductForm',

	controller: 'ProductForm',
	viewModel : {
		type: 'ProductForm'
	},

	bind: {
		title: '{rec.Name}'
	},

    layout: 'form',

	items: [
		{
			xtype     : 'textfield',
			fieldLabel: 'Name',
			bind      : '{rec.Name}'
		},
		{
			xtype     : 'textarea',
			fieldLabel: 'Description',
			height    : 100,
			bind      : '{rec.Description}'
		},
		{
			xtype     : 'numberfield',
			fieldLabel: 'Quantity',
			bind      : '{rec.Quantity}'
		},
		{
			xtype           : 'numberfield',
			fieldLabel      : 'Price',
			decimalPrecision: 2,
			bind            : '{rec.Price}'
		}
	],
	buttons : [
		{
			text     : 'Save',
			listeners: {
				click: 'onSave'
			}
		},
		{
			text     : 'Cancel',
			listeners: {
				click: 'onCancel'
			}
		}
	],

    closeForm: function(){
        console.log('this.closeform()');
        this.close();
        this.destroy();
    }
});
