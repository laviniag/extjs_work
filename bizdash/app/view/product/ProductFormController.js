Ext.define('BizDash.view.product.ProductFormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.ProductForm',

	onSave: function(btn){
		var productModel = this.getView().getViewModel().getData().rec;
		var productForm = this.getView();

        productModel.save({					 // save the record to the server
			success: function(user) {
				Ext.Msg.alert('Success', 'User saved successfully.', function(){
					productModel.commit();
					productForm.closeForm();
				});
			},
			failure: function(user) {
				Ext.Msg.alert('Failure', 'Failed to save user.', function(){
					productModel.reject();
					productForm.closeForm();
				});
			}
		});
	},

	onCancel: function(btn){
		var productModel = this.getView().getViewModel().getData().rec;
		productModel.reject();
        var productForm = this.getView();
        productForm.closeForm();
	}
});
