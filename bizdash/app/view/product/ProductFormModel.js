	Ext.define('BizDash.view.product.ProductFormModel', {
	    extend: 'Ext.app.ViewModel',

	    alias: 'viewmodel.ProductForm',

	    data: {
	        rec: null
	    },

        proxy: {
            type: 'ajax',
            api: {
                update: 'proxy/products.php'
            },
            writer: {
                type          : 'json',
                writeAllFields: true
            }
        }
	});