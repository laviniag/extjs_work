Ext.define('BizDash.view.product.ProductGrid', {
	extend: 'Ext.grid.Panel',

	requires: [
		'BizDash.view.product.ProductGridController'
	],
    title: 'Products List',

	xtype: 'product-ProductGrid',

	controller: 'ProductGrid',

	store  : 'Products',
	columns: [
		{ text: 'Name', dataIndex: 'Name' },
		{ text: 'Description', dataIndex: 'Description', flex: 1 },
		{
			text     : 'Quantity',
			dataIndex: 'Quantity',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				var colour = 'black';

				if (value <= 10) {
					colour = 'red';
				} else if (value <= 100) {
					colour = 'orange';
				}

				return '<span style="color: ' + colour + ';">' + value + '</span>';
			}
		},
		{
			xtype    : 'templatecolumn',
			width    : 200,
			text     : 'Price ',
			dataIndex: 'Price2',
			tpl      : '&euro; {Price2} <span class="stock-value" style="font-size:0.8em; color: #999;">'
				+'(&dollar; {StockValueConvert})</span>'
		},
		{
			xtype : 'widgetcolumn',
			width : 100,
			text  : 'Action',
			widget: {
				xtype    : 'button',
				text     : 'Details',
				listeners: {
					click: 'onDetailsClick'
				}
			}
		},
		{
			xtype    : 'widgetcolumn',
            width    : 200,
            text     : 'Historic Sales',
			dataIndex: 'HistoricSales',
			widget   : {
				xtype: 'sparklinebullet'
			}
		}
	]
});
