Ext.define('BizDash.view.user.UsersView', {
	extend: 'Ext.view.View',

	alias: 'widget.user-UsersView',
    requires: [
        'BizDash.view.user.UsersViewController'
    ],
    controller: 'UsersView',
	store: 'Users',

	tpl: [
		'<tpl for=".">',
		'   <div class="user-item">',
		'       <img src="resources/images/{Photo}" />',
		'       <div class="name">{Name}</div>',
		'       <div class="role">{Role}</div>',
		'       <button class="awesomelink">Delete</button>',
		'   </div>',
		'</tpl>'
	].join(''),

	itemSelector: '.user-item'
});