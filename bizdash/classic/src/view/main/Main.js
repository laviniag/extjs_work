/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BizDash.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'BizDash.view.main.MainController',
        'BizDash.view.main.MainModel',
        'BizDash.view.chart.BarChart',
        'BizDash.view.chart.SiteVisits',
        'BizDash.view.chart.StockLevelPie'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'product-ProductGrid'
        }]
    }, {
        title: 'Users',
        iconCls: 'fa-user',
        items: [{
            xtype: 'user-UsersView'
        }]
    }, {
        title: 'Groups',
        iconCls: 'fa-users',
        bind: {
            html: '<h2>GROUPS</h2>'
        },
        tbar: [{
            xtype: 'button',
            text: 'PRESS'
        }]
    }, {
        title: 'Settings',
        iconCls: 'fa-cog',
        bind: {
            html: '<h2>Settings</h2>'
        }
    }, {
        title: 'Categories',
        iconCls: 'fa-folder',
        items: [{
            xtype: 'chart-BarChart'
        }]
    }, {
        title: 'Visitors',
        iconCls: 'fa-users',
        items: [{
            xtype: 'chart-SiteVisits'
        }]
    }, {
        title: 'Products',
        iconCls: 'fa-gear',
        items: [{
            xtype: 'chart-StockLevelPie'
        }]
    }],

    listeners: {
        select: 'onItemSelected',
        afterrender: 'setBackground'
    },

    getButton: function () {
        return this.query('button[text="PRESS"]')[0];
    }
});
