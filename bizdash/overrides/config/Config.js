Ext.define('Overrides.config.Config', {
    override: 'BizDash.config.Config',
    getBuildNumber: function() {
        return 'Build Number: ' + this.callParent(arguments);
    }
});