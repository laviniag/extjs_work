<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "bizdash";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $record = json_decode(file_get_contents('php://input'));
    if (sizeof($record) > 0) {
        $keys = $values = $param = array();
        foreach ($record as $key => $value) {
            if ($key <> 'id' && $value <> '') {
                $keys[] = $key;
                $values[] = $value;
                $param[] = '?';
            }
        }

        $stmt = $conn->prepare('INSERT INTO users ('.implode(',',$keys).
            ') VALUES ('.implode(',',$param).')');
        for($ind = 0; $ind < count($values); $ind++) {
            $stmt->bindParam($ind+1, $values[$ind]);
        }
        $stmt->execute();
    }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;