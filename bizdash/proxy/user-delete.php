<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "bizdash";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    echo $_SERVER['REQUEST_METHOD'];
    $record = json_decode(file_get_contents('php://input'));
    if (sizeof($record) > 0) {
        $stmt = $conn->prepare('DELETE FROM users WHERE id = ?');
        $stmt->bindParam(1, $record->id);
        $stmt->execute();
    }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;