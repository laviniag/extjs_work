/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'Calc.Application',

    name: 'Calc',

    requires: [
        // This will automatically load all classes in the Calc namespace
        // so that application classes do not need to require each other.
        'Calc.*'
    ],

    // The name of the initial view to create.
    mainView: 'Calc.view.main.Main'
});

// Ext.application({
//     name: 'Calc',
//     launch: function () {
//         Ext.create('Calc.view.main.Main').show();
//         //Will create the calculator as a floating, movable window within the browser
//     }
// });