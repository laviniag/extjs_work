/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Calc.Application', {
    extend: 'Ext.app.Application',

    name: 'Calc',

    requires: [
        'Calc.model.TodoList',
        'Calc.model.Product',
        'Calc.model.Order',
        'Calc.model.Picture',
        'Calc.model.Chart1'
    ],

    views: [
        // TODO: add views here
        'calc.CalcView',
        'survey.SurveyForm',
        'todolist.TodoListView',
        'product.ProductGrid',
        'order.OrderGrid',
        'geo.GeoTree',
        'album.AlbumTree',
        'chart.ChartView'
    ],

    stores: [
        'TodoLists',
        'Products',
        'Orders',
        'TreeStore',
        'Albums',
        'Pictures',
        'Charts1'
    ],

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    launch: function() {

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
