Ext.define('Calc.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'Calc.model'
    }
});
