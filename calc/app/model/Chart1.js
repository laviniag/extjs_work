Ext.define('Calc.model.Chart1', {
    extend: 'Ext.data.Model',

    // fields: [
    //     'Country',
    //     'TotalOrders'
    // ]
    fields: [
        {
            name: 'year',
            type: 'string'
        },
        {
            name: 'population',
            type: 'int'
        }
    ]
});