Ext.define('Calc.model.Employee', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id',  type: 'int',  convert: null },
        { name: 'companyId', type: 'int', reference: 'Company' },
        { name: 'fullname', type: 'string' },
        { name: 'fulltime', type: 'boolean', defaultValue: true, convert: null },
        { name: 'gender', type: 'string' },
        { name: 'phone', type: 'string'},
    ],

    validators: {
        companyId: [
            { type: 'presence'}
        ],
        fullname:[
            { type: 'presence'},
            { type: 'length', min: 2 }
        ],
        phone: {
            type: 'format',
            matcher: '/^[(+{1})|(00{1})]+([0-9]){7,10}$/'
        },
        gender: {
            type: 'inclusion',
            list: ['Male', 'Female']
        }
    }
});