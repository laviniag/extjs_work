/**
 * sencha generate model Order
 */
Ext.define('Calc.model.Order', {
	extend: 'Ext.data.Model',

	fields: [
        { name: 'Id', type: 'int'  },
		{ name: 'Customer', type: 'string' },
		{ name: 'City', type: 'string' },
		{ name: 'Country', type: 'string' },
		{ name: 'OrderDate', type: 'date' },
		{ name: 'TotalAmount', type: 'float' },
		{ name: 'Year', type: 'date',
    		convert: function(val, rec) {
                return Ext.Date.format(rec.get('OrderDate'), "Y");
            },
            depends: ['OrderDate']
		},
	]
});
