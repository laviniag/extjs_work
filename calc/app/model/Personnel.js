Ext.define('Calc.model.Personnel', {
    extend: 'Calc.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
