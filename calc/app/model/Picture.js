Ext.define('Calc.model.Picture', {
    extend: 'Ext.data.Model',

    fields: [
        'id',
        'url',
        'albumId'
    ]
});