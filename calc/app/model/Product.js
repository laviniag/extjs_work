/**
 * sencha generate model Product
 */
Ext.define('Calc.model.Product', {
	extend: 'Ext.data.Model',

	requires: [
		'Ext.data.validator.Presence',
		'Ext.data.validator.Length'
	],

	// proxy: {
	// 	type  : 'rest',
	// 	api: {
     //        read: 'server/products.php',
     //        update: 'server/products.php'
     //    },
	// 	reader: {
     //        type: 'json'
     //    },
     //    writer: {
     //        type : 'json'
     //    }
	// },

	fields: [
        { name: 'Id', type: 'int'  },
		{ name: 'ProductName', type: 'string' },
		{ name: 'SupplierId', type: 'int' },
		{ name: 'UnitPrice', type: 'float' },
		{ name: 'Package', type: 'string' },
		{ name: 'IsDiscontinued', type: 'boolean' }
	],

	validators: {
        ProductName    : [
			{ type: 'presence'	},
			{ type: 'length',	min : 3	}
		],
        UnitPrice: 'presence'
	}
});
