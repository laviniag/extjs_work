Ext.define('Calc.model.TodoList', {
    extend: 'Ext.data.Model',

    alias: 'viewmodel.TodoList',

    fields: [
        {   name: 'id', type: 'string'  },
        {   name: 'employeeId', type: 'int'    },
        {   name: 'descr', type: 'string'    },
        {   name: 'day', type: 'string'    },
        {   name: 'done', type: 'boolean'   }
    ],
    sorters: [{
        property: 'done',
        direction: 'ASC'
    }],

    proxy: {
        type: 'rest',
        url: 'server/todolist.php',
        reader: {
            type: 'json',
        },
        writer: {
            type: 'json'
        }
    }
});
