Ext.define('Calc.store.Albums', {
    extend: 'Ext.data.TreeStore',
	storeId: 'Albums',

    root: {
        expanded: true,
        children: [
            
            { id: 1, text: ' California', expanded: true, children: [
                { id: 6, text: ' Big Sur', leaf: true},
                { id: 5, text: ' Yosemite', leaf: true}
            ] },
           { id: 4, text: ' Arizona', expanded: true, children: [
                { id: 3, text: ' Horseshoe bend', leaf: true }
            ] },
            { id: 2, text: ' Home', leaf: true },
            { id: 7, text: ' India', expanded: true, children: [
                { id: 8, text: ' Ooty', leaf: true },
                { id: 9, text: ' Chennai', leaf: true},
                { id: 10, text: ' Munnar', leaf: true },
            ] },
        ]
    }
});
