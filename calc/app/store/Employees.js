Ext.define('Calc.store.Employees', {
    extend: 'Ext.data.Store',

    model: 'Calc.model.Employee',

    storeId: 'storeEmployee',

    autoLoad: true,
    autoSync: true,

    proxy: {
        type  : 'rest',
        // url   : 'users/index.php',
        // type: 'ajax',
        api   : {
            create : 'server/employees.php',
            read   : 'server/employees.php',
            update : 'server/employees.php',
            destroy: 'server/employees.php'
        },
        reader: {
            type        : 'json',
            rootProperty: 'data'
        },
        writer: {
            type          : 'json',
            writeAllFields: true
        }
    }

});