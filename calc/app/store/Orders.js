/**
 * Calc.store.Orders
 */
Ext.define('Calc.store.Orders', {
	extend: 'Ext.data.Store',

	model: 'Calc.model.Order',

    storeId: 'Orders',
    pageSize: 10,

	autoLoad: true,

    proxy: {
        type  : 'rest',
        api: {
            read: 'server/orders.php'
        },
        reader: {
            type: 'json'
        },
        writer: {
            type          : 'json',
            writeAllFields: true
        },
        remoteSort: true,
        enablePaging: true
    }

});