Ext.define('Calc.store.Pictures', {
    extend: 'Ext.data.Store',

    storeId: 'Pictures',
    model: 'Calc.model.Picture',

    autoLoad: true,

    proxy: {
        type: 'rest',
        api: {
            read: 'server/pictures.php',
            update: 'server/pictures.php'
        },
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json'
        }
    }
});
