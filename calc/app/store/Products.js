/**
 * Calc.store.Products
 */
Ext.define('Calc.store.Products', {
	extend: 'Ext.data.Store',

	model: 'Calc.model.Product',

    storeId: 'Products',
    pageSize: 10,

	autoLoad: true,

    proxy: {
        type  : 'rest',
        api: {
            read: 'server/products.php',
            update: 'server/products.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'total'
        },
        writer: {
            type          : 'json',
            writeAllFields: true
        },
        remoteSort: true,
        enablePaging: true
    }

});