Ext.define('Calc.store.TodoLists', {
    extend: 'Ext.data.Store',

    model: 'Calc.model.TodoList',

    storeId: 'TodoLists',

    autoLoad: true,
    // autoSync: true,

    proxy: {
        type  : 'rest',
        api   : {
            create : 'server/todolist.php',
            read   : 'server/todolist.php',
            update : 'server/todolist.php',
            destroy: 'server/todolist.php'
        },
        reader: {
            type        : 'json',
            rootProperty: 'data'
        },
        writer: {
            type          : 'json',
            writeAllFields: true
        }
    }

});