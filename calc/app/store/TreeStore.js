Ext.define('Calc.store.TreeStore', {
    extend: 'Ext.data.TreeStore',

    root: {
        expanded: true,
        text: 'Continents',
        children: [{
            name: 'Antarctica',
            population: 0,
            area: 14,
            leaf: true
        },{
            name: 'South America',
            population: 385 ,
            area: 17.84,
            expanded: false,
            children: [{
                name: 'Chile',
                population: 18,
                area: 0.7,
                leaf: true,
            }]
        },{
            name: 'Asia',
            expanded: true,
            population: 4164,
            area: 44.57,
            children: [{
                name: 'India',
                leaf: true,
                population: 1210,
                area: 3.2
            },{
                name: 'China',
                leaf: true,
                population: 1357,
                area: 9.5
            }
            ]
        },{
            name: 'Africa',
            leaf: true,
            population: 1110,
            area: 30
        }
    ]}
});