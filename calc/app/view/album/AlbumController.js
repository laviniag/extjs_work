Ext.define('Calc.view.album.AlbumController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.Album',

    views: ['Calc.view.album.AlbumTree'],
    requires: [
    	'Calc.store.Pictures',
		'Calc.store.Albums'
	],

    init: function ()
    {
        // Configure the pics as draggable
        var pics = Ext.select('.thumb');
        Ext.each(pics.elements, function(el) {
            var dd = Ext.create('Ext.dd.DD', el, 'picsDDGroup', {
                isTarget  : false
            });
            Ext.apply(dd, this.onDragDrop);
        });

        var albums = Ext.select('td');
        Ext.each(albums.elements, function(el) {
            var albumDDTarget = Ext.create('Ext.dd.DDTarget', el, 'picsDDGroup');
        });
    },

    onDragDrop : function(evtObj, targetElId) {
	    Ext.Msg.alert(targetElId);
        var dropEl = Ext.get(targetElId);

        if (this.el.dom.parentNode.id != targetElId) {
            dropEl.appendChild(this.el);
            this.onDragOut(evtObj, targetElId);
            this.el.dom.style.position ='';
            this.el.dom.style.top = '';
            this.el.dom.style.left = '';
        }
        else {
            this.onInvalidDrop();
        }
    },
    onInvalidDrop : function() {
        this.invalidDrop = true;
    },

    onNodeSelect: function(node, rec, item, index, e)
    {
        var albums = [];
        albums.push(rec.id);
        rec.childNodes.forEach(function(item){
            albums.push(item.id);
        });

        Ext.getStore('Pictures').filter({
            property: 'albumId',
            operator: 'in',
            value   : albums
        });
    }
});
