Ext.define('Calc.view.album.AlbumTree', {
	extend: 'Ext.panel.Panel',

	requires: [
		'Calc.view.album.AlbumController'
	],
    title: 'Picture Explorer',

	xtype: 'app-AlbumTree',
	controller: 'Album',

    width: 1200,
    height: 800,

    items: [{
        xtype: 'container',
        layout: 'hbox',

        cls: 'pics-list',
        items: [{
            xtype: 'treepanel',
            width: 200,

            height: '100%',
            store: 'Albums',
            border: true,
            useArrows: true,
            cls: 'tree',
            rootVisible: false,
            listeners:{
                itemclick: 'onNodeSelect'
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                ui: 'footer',
                items: [
                    { xtype: 'component', flex: 1 },
                    { xtype: 'button', text: 'Upload', cls: 'btn-blue' }
                ]
            }]

        },
            {
                xtype:'dataview',
                reference: 'picsList',
                cls:'pics-list-content',
                scrollable: true,
                store: 'Pictures',
                tpl: [
                    '<tpl for=".">',
                    '<div class="thumb"><img  src="{url}" title=""></div>',
                    '</tpl>'
                ],
                multiSelect: true,
                minHeight: 800,
                flex:1,
                trackOver: true,
                overItemCls: 'x-item-over',
                itemSelector: 'div.thumb',
                emptyText: 'No images to display'
            }
        ]
    }]
});