/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('Calc.view.calc.CalcModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.calc',

    data: {
        display: 0.0
    }
});