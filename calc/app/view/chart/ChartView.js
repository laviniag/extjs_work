Ext.define('Calc.view.chart.ChartView', {
    extend: 'Ext.chart.Chart',

    title: 'Orders Chart',
    xtype: 'app-ChartView',

    height: 700,
    // width: 1200,
    layout: 'fit',

    items: [{
        xtype: 'chart',
        insetPadding: { top: 60, bottom: 20, left: 20, right: 40 },
        animate: true,
        store: 'Charts1',
        // axes: [{
        //     type: 'numeric',
        //     position: 'left',
        //     grid: true,
        //     title: { text: 'Population in Millions', fontSize: 16 },
        // }, {
        //     type: 'category',
        //     title: { text: 'Year', fontSize: 16 },
        //     position: 'bottom',
        // }
        // ],
        // series: [{
        //     type: 'bar',
        //     xField: 'year',
        //     yField: ['population']
        // }],
        // sprites: {
        //     type: 'text',
        //     text: 'United States Population',
        //     font: '25px Helvetica',
        //     width: 120,
        //     height: 35,
        //     x: 100,
        //     y: 40
        // }

        axes: [
            {
                type    : 'numeric',
                position: 'left',
                fields  : ['population'],
                title   : 'Population',
                grid: true
                // minimum       : 0,
                // maximum       : 300000,
                // majorTickSteps: 30000
            },
            {
                type    : 'category',
                position: 'bottom',
                fields  : ['year'],
                title   : 'Year'
            }
        ],
        series : [
            {
                type  : 'bar',
                axis  : 'bottom',
                xField: 'year',
                yField: 'population'
            }
        ]
        // sprites: {
        //     type: 'text',
        //     text: 'United States Population',
        //     font: '25px Helvetica',
        //     width: 120,
        //     height: 35,
        //     x: 100,
        //     y: 40
        // }
    }]
});