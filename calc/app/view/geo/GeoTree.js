Ext.define('Calc.view.geo.GeoTree', {
	extend: 'Ext.tree.Panel',

    title: 'Geo Statistics',

	xtype: 'app-GeoTree',
	store  : 'TreeStore',

    width: 500,
    height: 450,
    rootVisible: false,
    useArrows: true,
    lines: false,
    scope: this,
    columns: [{
        xtype: 'treecolumn',
        text: 'Name',
        flex: 1,
        sortable: true,
        dataIndex: 'name'
    },{
        text: 'Population (millons)',
        sortable: true,
        width: 150,
        dataIndex: 'population'
    },{
        text: 'Area (millons km^2)',
        width: 150,
        sortable: true,
        dataIndex: 'area'
    }]
});
