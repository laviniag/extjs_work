Ext.define('Calc.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',
    views: ['Calc.view.calc.CalcView'],
    models: ['Main'],

});