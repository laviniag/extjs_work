Ext.define('Calc.view.order.OrderController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.Order',

    requires: [
        'Ext.window.MessageBox'
    ],

	onDetailsClick: function(btn) {
		Ext.Msg.alert("Products", "You clicked !");
	},

});
