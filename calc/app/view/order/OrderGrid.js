Ext.define('Calc.view.order.OrderGrid', {
	extend: 'Ext.grid.Panel',

	requires: [
		'Calc.view.order.OrderController'
	],
    title: 'Orders Recap',

	xtype: 'app-OrderGrid',
	controller: 'Order',
	store  : 'Orders',

    width: 800,
    enableLocking:  false,
    viewConfig: {
        trackOver: true,
        stripeRows: false
    },

    items: [{
        xtype: 'pivotgrid',
        reference: 'PivotOrderGrid',

        matrix: {
            leftAxis: [{
                width: 100,
                dataIndex: 'Customer',
                header: 'Customer'
            }],

            topAxis: [{
                dataIndex: 'Year',
                header: 'Year',
                direction: 'ASC'
            }],
            aggregate: [{
                measure: 'TotalAmount',
                header: 'Total Orders',
                aggregator: 'sum',
                align: 'right',
                width: 100,
                renderer: Ext.util.Format.numberRenderer('0,000.00')
            }]
        }
    }]
});
