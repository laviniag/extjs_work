Ext.define('Calc.view.product.ProductController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.Product',

    requires: [
        'Ext.window.MessageBox'
    ],

	onDetailsClick: function(btn) {
		Ext.Msg.alert("Products", "You clicked !");
	},

	onSaveClick: function(btn) {
        var store = Ext.getStore('Products');
        store.sync({
            success: function (batch, options) {
                Ext.Msg.alert("Products", "Grid has been updated");
            },
            scope: this
        });
	}
});
