Ext.define('Calc.view.product.ProductGrid', {
	extend: 'Ext.grid.Panel',

	requires: [
		'Calc.view.product.ProductController'
	],
    title: 'Products List',

	xtype: 'app-ProductGrid',
	controller: 'Product',
	store  : 'Products',

    width: 800,
    plugins: ['cellediting','gridfilters'],
    pageSize: 10,
    remoteSort: 'true', //For Remote Sorting

    // inline buttons
    buttons: [{text:'Save',
        listeners: {
            click: 'onSaveClick'
        }},
        {text:'Cancel'}],
    buttonAlign:'center',

    columns: [
        {   text: 'Id',
            dataIndex: 'Id',
            hidden: true
        },
        {   text: 'Name',
            dataIndex: 'ProductName',
            flex: 1,
            filter: 'string',
            listeners: {
                click: 'onDetailsClick'
            }
        },
        {   text: 'Price',
            dataIndex: 'UnitPrice',
            width: 120,
            renderer: Ext.util.Format.numberRenderer('$ 0.00'),
            editor: {
                allowBlank: false,
                type: 'float'
            }
        },
        {   text: 'Package',
            dataIndex: 'Package',
            width: 200,
            filter: 'string',
            sortable: false,
            editor: {
                allowBlank: false,
                type: 'string'
            }
        },
        {   text: 'Available',
            dataIndex: 'IsDiscontinued',
            filter: 'boolean',
            renderer: function(value) {
                if (value == 0) {
                    return '<span style="color:green;"> Yes </span>';
                } else {
                    return '<span style="color:red;"> No </span>';
                }
            },
            editor: new Ext.form.field.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                store: [
                    ['0','Yes'],
                    ['1','No']
                ]
            })
        }
	],
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: 'Products',
        pageSize: 10,
        id: 'paging',
        dock: 'bottom',
        displayInfo: true
    }]
});
