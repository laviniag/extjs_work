Ext.define('Calc.view.todolist.TodoListController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.TodoList',
    views: [
        'Calc.view.todolist.TodoListView'
    ],

    init: function ()
    {
        var me = this;
        Ext.getStore('TodoLists').load({
            params:  {
                alfa: 'false'
            },
            callback : function(records, operation, success) {
                console.log(records);
                Ext.each(records, function (record) {
                    //Add a container for each record
                    me.addToDoToView(record);
                });
            }
        });

        Ext.getBody().on('click', function (event, target) {
            me.onDelete(event, target);
        }, null, {
            delegate: '.fa-times'
        });

    },

    onAddToDo: function ()
    {
        var store = Ext.getStore('TodoLists');
        var dtime = new Date();
        
        var descr = this.lookupReference('newToDo').value.trim();
        if (descr != '') {
            store.add({
                descr: descr,
                day: [dtime.getFullYear(),dtime.getMonth(),dtime.getDay()].join('-')
            });
            store.sync({
                success: function (batch, options) {
                    this.lookupReference('newToDo').setValue('');
                    this.addToDoToView(options.operations.create[0]);
                },
                scope: this
            });
        }
    },

    addToDoToView: function (record)
    {
        this.view.add([{
            xtype: 'container',
            layout: 'hbox',
            cls: 'row',
            items: [{
                xtype: 'checkbox',
                boxLabel: record.get('day') +' - '+ record.get('descr'),
                checked: record.get('done'),
                flex: 1
            }, {
                html: '<a class="hidden" href="#"><i taskId="' + record.get('id') + '" class="fa fa-times"></i></a>',
            }]
        }]);
    },

    onDelete: function (event, target)
    {
        var store = Ext.getStore('TodoLists');
        var targetCmp = Ext.get(target);
        var id = targetCmp.getAttribute('taskId');
        store.remove(store.getById(id));
        store.sync({
            success: function () {
                this.view.remove(targetCmp.up('.row').id)
            },
            scope: this
        });
    }
});


