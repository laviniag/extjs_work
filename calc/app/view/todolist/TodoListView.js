Ext.define('Calc.view.todolist.TodoListView', {
    extend: 'Ext.panel.Panel',

    /* Marks these are required classes to be to loaded before loading this view */
    requires: [
        'Calc.view.todolist.TodoListController'
    ],
    title: 'To Do List',

    xtype: 'app-TodoList',
    controller: 'TodoList',
    store  : 'TodoLists',

    items: [{
        xype: 'container',
        items: [ 
        {
            xtype: 'container',
            layout: 'hbox',
            cls: 'task-entry-panel',
            defaults: {
                flex: 1
            },
            items: [
                {
                    reference: 'newToDo',
                    xtype: 'textfield',
                    emptyText: 'Enter a new todo here'
                },
                {
                    xtype: 'button',
                    name: 'addNewToDo',
                    cls: 'btn-orange',
                    text: 'Add',
                    maxWidth: 50,
                    handler: 'onAddToDo'
                }]
            }
        ]
    }]
});