<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "common";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    if (isset($_GET['type'])) {
        $type = $_GET['type'];
        switch ($type) {
            case 1:
$query = <<<SQL
SELECT Country, SUM(TotalAmount) AS TotalOrders FROM orders
INNER JOIN customers ON customers.Id = orders.CustomerId WHERE CustomerId <= 10;
SQL;
                $stmt = $conn->prepare($query);
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetchAll();
                foreach ($result as &$row) {
                    $row['TotalOrders'] = intval($row['TotalOrders']);
                }
                break;

            case 2:

                break;
        }
        echo json_encode($result);
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;