<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "common";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $method = $_SERVER['REQUEST_METHOD'];
$query = <<<SQL
SELECT customers.Id, CONCAT(FirstName,' ',LastName) AS Customer, City, Country, OrderDate, TotalAmount
FROM customers INNER JOIN orders ON customers.Id = orders.CustomerId WHERE customers.Id <= 40;
SQL;

    switch ($method) {
        case 'GET':

            $stmt = $conn->prepare($query);
            $stmt->execute();
            // set the resulting array to associative
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // normalize to utf_8 charset
            foreach ($result as &$row) {
                $row['Customer'] = utf8_encode($row['Customer']);
                $row['City'] = utf8_encode($row['City']);
            }
            echo json_encode($result);
            break;

        case 'PUT':
            $record = json_decode(file_get_contents('php://input'));
            $product = json_decode(file_get_contents('models/product.json'));

            $params = $values = array();
            foreach ($record as $key => $value) {
                if (property_exists($product,$key)) {
                    $params[] = $key.'= ?';
                    $values[] = $value;
                }
            }
            if (! empty($values)) {
                $query = "UPDATE orders SET ".implode(',', $params)." WHERE id = ?";
                $stmt = $conn->prepare($query);
                for ($ind = 0; $ind < count($values); $ind++) {
                    $stmt->bindParam($ind + 1, $values[$ind]);
                }
                $stmt->bindParam($ind + 1, $record->id);
                $stmt->execute();
            }
            break;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;