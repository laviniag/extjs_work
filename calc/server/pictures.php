<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */

$method = $_SERVER['REQUEST_METHOD'];
try {
    switch ($method) {
        case 'GET':
            $stack = array();
            $images = scandir('../resources/images/thumbs');
            $ind = 1;
            foreach ($images as $image) {
                if (strpos($image,'jpg') > 0) {
                    $stack[] = ['id' => $ind++,
                                'url' => './resources/images/thumbs/'.$image,
                                'albumId' => rand(1,10) ];
                }
            }
            echo json_encode($stack);
            break;

        case 'PUT':

            break;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;