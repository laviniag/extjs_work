<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "common";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
//    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::NULL_EMPTY_STRING);

    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $method = $_SERVER['REQUEST_METHOD'];

    switch ($method) {
        case 'GET':
            if(isset($_GET['id'])) {
                $id = $_GET['id'];
                $stmt = $conn->prepare("SELECT * FROM products WHERE id = $id");
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_OBJ);
                $result = $stmt->fetchAll();
            }
            else {
                $limit = 10;    $start = 0;
                if(isset($_GET['limit']) && isset($_GET['start'])) {
                    $limit = $_GET['limit'];
                    $start = $_GET['start'];
                }
                $stmt = $conn->prepare("SELECT COUNT(*) AS total FROM products");
                $stmt->execute();
                $response = $stmt->fetch(PDO::FETCH_OBJ);
                $total = $response->total;

                $stmt = $conn->prepare("SELECT * FROM products LIMIT ? OFFSET ?");
                $stmt->bindParam(1, $limit, PDO::PARAM_INT);
                $stmt->bindParam(2, $start, PDO::PARAM_INT);
                $stmt->execute();

                // set the resulting array to associative
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

                // normalize to utf_8 charset
                foreach ($rows as &$row) {
                    $row['ProductName'] = utf8_encode($row['ProductName']);
                }
                $result = ['total' => $total, 'data' => $rows];
            }
            echo json_encode($result);
            break;

        case 'PUT':
            $record = json_decode(file_get_contents('php://input'));
            $product = json_decode(file_get_contents('models/product.json'));

            $bind = ['string' => PDO::PARAM_STR, 'int' => PDO::PARAM_INT,
                     'float' => PDO::PARAM_STR, 'bool' => PDO::PARAM_BOOL];
            $params = $values = $types = array();
            foreach ($record as $key => $value) {
                if (property_exists($product,$key)) {
                    $params[] = $key.'= ?';
                    $values[] = $value;
                }
            }
//            var_dump($values);
            if (! empty($values)) {
                $query = "UPDATE products SET ".implode(',', $params)." WHERE Id = ?";
                $stmt = $conn->prepare($query);
                for ($ind = 0; $ind < count($values); $ind++) {
                    $stmt->bindParam($ind + 1, $values[$ind]);
                }
                $stmt->bindParam($ind + 1, $record->Id);
                $stmt->execute();
            }
            break;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;