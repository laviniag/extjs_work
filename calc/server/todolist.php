<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 03/09/2017
 * Time: 19:41
 */
$server = "localhost";
$dbname = "common";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $method = $_SERVER['REQUEST_METHOD'];

    switch ($method) {
        case 'GET':
            if(isset($_GET['id'])) {
                $id = $_GET['id'];
                $stmt = $conn->prepare("SELECT *, CONVERT(dtime,DATE) AS day FROM todolist WHERE id = $id");
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_OBJ);
                $result = $stmt->fetchAll();
            }
            else {
                $stmt = $conn->prepare("SELECT *, CONVERT(dtime,DATE) AS day FROM todolist");
                $stmt->execute();

                // set the resulting array to associative
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetchAll();
            }
            echo json_encode($result);
            break;

        case 'POST':
            $record = json_decode(file_get_contents('php://input'));
            $todolist = json_decode(file_get_contents('models/todolist.json'));

            $keys = $params = $values = array();
            foreach ($record as $key => $value) {
                if (property_exists($todolist,$key) && $todolist->$key <> null) {
                    $keys[] = $key;
                    $params[] = '? ';
                    $values[] = $value;
                }
            }
            if (! empty($values)) {
                $stmt = $conn->prepare('INSERT INTO todolist ('.implode(',',$keys).
                            ') VALUES ('.implode(',',$params).')');
                for($ind = 0; $ind < count($values); $ind++) {
                    $stmt->bindParam($ind+1, $values[$ind]);
                }
                $stmt->execute();
            }
            break;

        case 'PUT':
            $record = json_decode(file_get_contents('php://input'));
            $product = json_decode(file_get_contents('models/product.json'));

            $params = $values = array();
            foreach ($record as $key => $value) {
                if (property_exists($product,$key)) {
                    $params[] = $key.'= ?';
                    $values[] = $value;
                }
            }
            if (! empty($values)) {
                $query = "UPDATE todolist SET ".implode(',', $params)." WHERE id = ?";
                $stmt = $conn->prepare($query);
                for ($ind = 0; $ind < count($values); $ind++) {
                    $stmt->bindParam($ind + 1, $values[$ind]);
                }
                $stmt->bindParam($ind + 1, $record->id);
                $stmt->execute();
            }
            break;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;