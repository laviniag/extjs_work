/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'Expense.Application',

    name: 'Expense',

    requires: [
        // This will automatically load all classes in the Expense namespace
        // so that application classes do not need to require each other.
        'Expense.*'
    ],

    // The name of the initial view to create.
    mainView: 'Expense.view.main.Main'
});
