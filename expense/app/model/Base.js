Ext.define('Expense.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'Expense.model'
    }
});
