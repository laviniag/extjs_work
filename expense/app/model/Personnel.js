Ext.define('Expense.model.Personnel', {
    extend: 'Expense.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
