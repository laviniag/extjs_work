Ext.define('Expense.store.ExpensebyMonth', {
    extend: 'Ext.data.Store',

    storeId: 'ExpensebyMonth',
    autoLoad: true,

    fields: [{name:'date', type: 'string'}, 'total'],
    data: (function () {
        var data = [];
        var expense = Ext.create('Expense.store.Expense');
        expense.group('date');
        var groups = expense.getGroups();

        groups.each(function (group) {
            var dt = new Date(group.config.groupKey);
            var label = ["Jan","Feb","Mar","Apr","May"][dt.getMonth()]+'/'+dt.getFullYear();
            data.push({ date: label, total: group.sum('spent') });
            console.log(label);
            console.log(group.sum('spent'));
        });
        // console.log(data);
        return data;
    })()
});