Ext.define('Expense.view.main.Bar', {
        extend: 'Ext.chart.CartesianChart',

        requires: ['Ext.chart.axis.Category',
                'Ext.chart.series.Bar3D',
                'Ext.chart.axis.Numeric',
                'Ext.chart.interactions.ItemHighlight'],

        xtype: 'mainbar',
        height: 600,
        width: 1200,
        layout: 'fit',
        padding: { top: 40, bottom: 20, left: 20, right: 20 },
        legend: { docked: 'top' },
        // insetPadding: { top: 100, bottom: 20, left: 20, right: 40 },

        store: 'ExpensebyMonth',
        // axes: [{
        //     type: 'numeric3d',
        //     position: 'left',
        //     fields  : ['total'],
        //     grid: true,
        //     title: { text: 'Spendings in $', fontSize: 16 },
        //     minimum: 0
        //
        // }, {
        //     type: 'category3d',
        //     title: { text: 'Month', fontSize: 16 },
        //     position: 'bottom',
        //     fields  : 'date',
        //     label: {
        //             font: 'bold Arial',
        //             rotate: { degrees: 300 },
        //     }
        //         // renderer: function (date) {
        //         //         return ["Jan", "Feb", "Mar", "Apr", "May"][date.getMonth()];
        //         // },
        // }],
        // series: [{
        //     type: 'bar3d',
        //     xField: 'date',
        //     yField: ['total'],
        //     stacked: false
        // }]

        // axes: [
        //         {
        //             type    : 'numeric',
        //             position: 'left',
        //             fields  : ['total'],
        //             title: { text: 'Spendings in $', fontSize: 16 },
        //             grid: true,
        //             minimum       : 0
        //             // maximum       : 300000,
        //             // majorTickSteps: 30000
        //         },
        //         {
        //             type    : 'time',
        //             position: 'bottom',
        //             fields  : ['date'],
        //             title   : 'Date'
        //         }
        //     ],
        //     series : [
        //         {
        //             type  : 'bar',
        //             // axis  : 'left',
        //             xField: 'date',
        //             yField: 'total'
        //         }
        //     ],
        // sprites: [{
        //         type: 'text',
        //         text: 'Expense by Month',
        //         font: '20px Helvetica',
        //         width: 120,
        //         height: 35,
        //         x: 60,
        //         y: 40
        // }]

    // store: {
    //     fields: ['name', 'g1', 'g2'],
    //     data: [
    //         {"name": "Item-0", "g1": 57, "g2": 59},
    //         {"name": "Item-1", "g1": 45, "g2": 50},
    //         {"name": "Item-2", "g1": 67, "g2": 43},
    //         {"name": "Item-3", "g1": 45, "g2": 18},
    //         {"name": "Item-4", "g1": 30, "g2": 90}
    //     ]
    // },
    //define the x and y-axis configuration.
    // axes: [{
    //     type: 'numeric',
    //     title: 'Total',
    //     position: 'bottom',
    //     grid: true,
    //     minimum: 0
    // }, {
    //     type: 'category',
    //     title: 'Date',
    //     position: 'left',
    //     renderer: function (date) {
    //         return date.getMonth();
    //     }
    // }],
    //
    // //define the actual bar series.
    // series: [{
    //     type: 'bar',
    //     xField: 'date',
    //     yField: ['total']
    //     // axis: 'bottom'
    // }]

    axes: [{
        type: 'numeric3d',
        position: 'left',
        fields: ['total'],
        title: {
            text: 'Total',
            fontSize: 15
        },
        grid: {
            odd: {
                fillStyle: 'rgba(255, 255, 255, 0.06)'
            },
            even: {
                fillStyle: 'rgba(0, 0, 0, 0.03)'
            }
        }
    }, {
        type: 'category3d',
        position: 'bottom',
        title: {
            text: 'Date',
            fontSize: 15
        },
        fields: 'date'
    }],
    series: {
        type: 'bar3d',
        xField: 'date',
        yField: ['total']
    }
});
