Ext.define('Expense.view.main.Pie', {
        extend: 'Ext.chart.PolarChart',

        // requires: ['Ext.chart.series.Pie3D'],
        xtype: 'mainpie',

    // items: [{
    //     xtype: 'combo',
    //     fieldLabel: 'Select Month',
    //     name:'theName',
    //     queryMode:'local',
    //     store: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
    //     displayField:'Display Field',
    //     autoSelect:true,
    //     forceSelection:true,
    //     listeners: {
    //         select: 'onMonthSelect'
    //     }
    // }, {
        height: 600,
        legend: { docked: 'bottom' },
        insetPadding: { top: 40, bottom: 20, left: 10, right: 10 },
        listeners: {
            beforerender: function () {
                var dateFiter = new Ext.util.Filter({
                    filterFn: function(item) {
                        return item.data.date.getMonth() == 0;
                    }
                });
                Ext.getStore('Expense').addFilter(dateFiter);
            },
            select: 'onMonthSelect'
        },
        store: 'Expense',
        series: [{
            type: 'pie3d',
            donut: 50,
            thickness: 80,
            distortion: 0.6,
            angleField: 'spent',
            label: {
                field: 'cat'
            }
        }]
    // }]
});
