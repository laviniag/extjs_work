/**
 * This view is an example list of people.
 */
Ext.define('Expense.view.main.List', {
    extend: 'Ext.grid.Panel',

    xtype: 'mainlist',
    maxHeight: 400,
    requires: [ 'Expense.store.Expense'],
    title: 'Year to date expense by category',
    store: 'Expense',

    columns: {
        defaults: { flex:1 },
        items: [{
            text: 'Category',
            dataIndex: 'cat'
        }, {
            formatter: "date('F')",
            text: 'Month',
            dataIndex: 'date'
        }, {
            text: 'Spent',
            dataIndex: 'spent'
        }]
    }
});
