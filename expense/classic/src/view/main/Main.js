/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Expense.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Expense.view.main.MainController',
        'Expense.view.main.List'
        // 'Expense.view.main.Bar',
        // 'Expense.view.main.Pie'
    ],

    controller: 'main',
    viewModel: 'main',
    autoScroll: true,

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        items: [{
            xtype: 'mainlist'
        }]
    },
    {
        title: 'Bar Chart',
        iconCls: 'fa-bar-chart',
        items: [{
            xtype: 'mainbar'
        }]
    },
    {
        title: 'Pie Chart',
        iconCls: 'fa-pie-chart',
        items: [{
            xtype: 'mainpie'
        }]
    }]

    // Truncated code

    // items: [{
    //     title: 'Year to Date',
    //     iconCls: 'fa-bar-chart',
    //
    //         { xtype: 'mainlist'},
    //         { xtype: 'mainbar' }
    //     ]
    // },
    //     {
    //         title: 'By Month',
    //         iconCls: 'fa-pie-chart',
    //         items: [{
    //             xtype: 'combo',
    //             value: 'Jan',
    //             fieldLabel: 'Select Month',
    //             store: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
    //             listeners: {
    //                 select: 'onMonthSelect'
    //             }
    //         }, {
    //             xtype: 'mainpie'
    //         }]
    //     }]
});