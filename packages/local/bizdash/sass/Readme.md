# bizdash/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    bizdash/sass/etc
    bizdash/sass/src
    bizdash/sass/var
