# theme-bizdash/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme-bizdash/sass/etc
    theme-bizdash/sass/src
    theme-bizdash/sass/var
