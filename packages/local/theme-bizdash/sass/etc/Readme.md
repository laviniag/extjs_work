# theme-bizdash/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme-bizdash/sass/etc"`, these files
need to be used explicitly.
